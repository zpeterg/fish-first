FROM php:7.2-apache

# RUN apt-get update && apt-get install -y \
#   curl 

ENV PORT 8080

COPY src/ /var/www/html/

CMD sed -i "s/80/$PORT/g" /etc/apache2/sites-available/000-default.conf /etc/apache2/ports.conf && docker-php-entrypoint apache2-foreground